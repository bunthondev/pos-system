FROM php:7.4-fpm

# https://github.com/nodesource/distributions/blob/master/README.md
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -

RUN apt-get -y update && apt-get install -yqq gnupg unzip libz-dev libmemcached-dev cron \
    libpng-dev libicu-dev libxml2 git libcurl4-gnutls-dev libmcrypt-dev libvpx-dev libjpeg-dev supervisor \
    libxpm-dev zlib1g-dev libxml2-dev libexpat1-dev libbz2-dev libgmp3-dev libldap2-dev unixodbc-dev \
    libjpeg62-turbo-dev libpq-dev libsqlite3-dev libaspell-dev libsnmp-dev libpcre3-dev libtidy-dev gcc make \
    autoconf libc-dev libzip-dev libonig-dev pkg-config nodejs vim

RUN npm install -g yarn

RUN pecl install memcached && echo extension=memcached.so >> /usr/local/etc/php/conf.d/memcached.ini \
&& pecl install redis && echo extension=redis.so >> /usr/local/etc/php/conf.d/redis.ini

RUN docker-php-ext-configure gd --with-jpeg \
&& docker-php-ext-install bcmath mbstring tokenizer pdo pdo_mysql mysqli curl gd intl zip json bz2 opcache xml dom

RUN pecl install -o -f redis \
&&  rm -rf /tmp/pear \
&&  docker-php-ext-enable redis

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
&& php composer-setup.php --install-dir=/var/www --version=2.0.8 \
&& php -r "unlink('composer-setup.php');" && ln -s /var/www/composer.phar /usr/bin/composer

COPY .config/local/php/change-user-mode.sh /usr/local/bin/change-user-mode.sh
COPY .config/local/php/php.ini /usr/local/etc/php/php.ini
COPY .config/local/php/www.conf /usr/local/etc/php-fpm.d/www.conf
COPY .config/local/php/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

COPY .config/local/php/crontab /etc/cron.d/crontab
RUN chmod 0644 /etc/cron.d/crontab

WORKDIR /var/www/html

COPY .config/local/php.ini /usr/local/etc/php/php.ini
COPY .config/local/php/supervisord/* /etc/supervisor/conf.d/

CMD ["/usr/bin/supervisord"]

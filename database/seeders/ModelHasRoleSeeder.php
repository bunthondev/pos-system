<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ModelHasRole;
use App\Models\User;
use Backpack\PermissionManager\app\Models\Role;

class ModelHasRoleSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $users = User::get();

        foreach($users as $user) {
            $role = Role::where('name', $user->name)->first();

            if ($role instanceof Role) {
                $user->assignRole($role->name);
            }
        }
    }
}

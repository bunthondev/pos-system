<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RoleHasPermission;
use Backpack\PermissionManager\app\Models\Permission;
use Backpack\PermissionManager\app\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $permissions = config('permissions');
        $result = [];

        foreach ($permissions as $key => $permission) {
            $result = array_merge_recursive($result, $permission);
        }

        foreach ($result as $key => $data) {
            $permission = Permission::firstOrCreate([
                'name' => $data['name'],
                'guard_name' => 'backpack'
            ]);
            $roles = $data['roles'];

            foreach ($roles as $roleName) {
                $role = Role::where('name', $roleName)->first();
                RoleHasPermission::firstOrCreate(
                    [
                        'permission_id' => $permission->id,
                        'role_id'       => $role->id
                    ]
                );
            }
        }
    }
}

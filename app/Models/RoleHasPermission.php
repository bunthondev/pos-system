<?php

namespace App\Models;

class RoleHasPermission extends BaseModel
{
    protected $table = 'role_has_permissions';
    public $timestamps = false;
}

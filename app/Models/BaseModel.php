<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Backpack\CRUD\app\Models\Traits\CrudTrait;
use App\Traits\CreatedBy;

class BaseModel extends Model
{
    use CrudTrait, CreatedBy;

    protected $guarded = ['id'];
}

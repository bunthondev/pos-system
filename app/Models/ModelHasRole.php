<?php

namespace App\Models;

class ModelHasRole extends BaseModel
{
    protected $table = 'model_has_roles';
    public $timestamps = false;
}
